import {HapiModule} from "./modules/hapi";
import {DatabaseModule} from "./modules/database";
import {Person} from "./models/Person";

export class App {
    private db: DatabaseModule = new DatabaseModule();
    private hapi: HapiModule = new HapiModule(this.db);

    constructor() {

    }

    public async launch() {
        await this.db.launch();
        await this.hapi.launch();
    }
}