import {Cursor, Db, MongoClient} from "mongodb";
import {Person} from "../models/Person";

export class DatabaseModule {
    private db: Db;

    public async launch() {
        let client: MongoClient = await MongoClient.connect("mongodb://localhost:27017/s5-prxproj-stundenapp");
        this.db = client.db("s5-prxproj-stundenapp");

        console.log(`[DBAS] Connected`);
    }

    public async getPeople(): Promise<Person[]> {
        let people = await this.db.collection("people").find().toArray();
        people.forEach((person) => delete person._id);

        return people;
    }

    public async getPerson(lastname: string): Promise<Person> {
        let person = await this.db.collection("people").findOne({ lastname });
        if(person)
            delete person._id;

        return person;
    }

    public async insertPerson(person: Person): Promise<void> {
        await this.db.collection("people").insertOne(person);
    }

    public async updatePersonSprintData(firstname: string, lastname: string, number: number, hours: number, remarks: string): Promise<void> {
        let person: Person = await this.getPerson(lastname);
        if(!person)
            await this.insertPerson({ firstname, lastname, sprints: [{ number, hours, remarks }]});
        else {
            // Check if data for given sprintId already exists
            let foundIndex = person.sprints.findIndex(o => o.number === number);
            if(foundIndex !== -1)
                person.sprints[foundIndex] = { number, hours, remarks };
            else
                person.sprints.push({ number, hours, remarks });

            await this.updatePerson(lastname, person);
        }
    }

    public async updatePerson(lastname: string, updatedPerson: Person): Promise<void> {
        let person: Person = await this.getPerson(lastname);
        if(!person)
            await this.insertPerson(updatedPerson);
        else
            await this.db.collection("people").updateOne({ lastname }, { $set: updatedPerson });
    }
}