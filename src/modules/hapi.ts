import {Server} from "@hapi/hapi";
import * as Hapi from "@hapi/hapi";
import * as Joi from "joi";
import {RootRoute} from "../routes/rootRoute";
import {DatabaseModule} from "./database";
import {PeopleRoute} from "../routes/peopleRoute";
import {PersonRoute} from "../routes/personRoute";

export class HapiModule {
    private server: Server = Hapi.server({
        host: "localhost",
        port: 3001,
        routes: {cors: {origin: ['*']}}
    });

    constructor(db: DatabaseModule) {
        this.server.validator(Joi);

        new RootRoute(this.server, db);
        new PeopleRoute(this.server, db);
        new PersonRoute(this.server, db);
    }

    public async launch() {
        await this.server.start();
        console.log(`[HAPI] Listening on ${this.server.info.protocol}://${this.server.info.host}:${this.server.info.port}`);
    }
}