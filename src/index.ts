import * as Hapi from "@hapi/hapi";
import { Server, ResponseToolkit, Request } from "@hapi/hapi";
import {App} from "./app";

async function init() {
    const app: App = new App();

    await app.launch();
}

init();