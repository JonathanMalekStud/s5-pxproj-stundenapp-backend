export interface Sprint {
    number: number;
    hours: number;
    remarks: string;
}

export interface Person {
    firstname: string;
    lastname: string;
    sprints: Sprint[];
}