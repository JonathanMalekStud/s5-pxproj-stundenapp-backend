import {Route} from "./route";
import {Request, ResponseToolkit, Server} from "@hapi/hapi";

export class RootRoute extends Route {
    public setupRoutes(): void {
        this.server.route({
            method: "GET",
            path: "/",
            handler: (req: Request, res: ResponseToolkit) => {
                return { message: "Hello World" };
            }
        });
    }
}