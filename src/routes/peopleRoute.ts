import * as Joi from "@hapi/joi";
import {Route} from "./route";
import {Request, ResponseToolkit} from "@hapi/hapi";
import {Person} from "../models/Person";

export class PeopleRoute extends Route {
    setupRoutes(): void {
        this.server.route({
            method: "GET",
            path: "/people",
            handler: async (req: Request, res: ResponseToolkit) => {
                return await this.db.getPeople();
            }
        });

        this.server.route({
            method: "POST",
            path: "/people",
            options: {
                validate: {
                    payload: Joi.object({
                        firstname: Joi.string().required(),
                        lastname: Joi.string().required(),
                        sprint: Joi.number().integer().min(0).required(),
                        hours: Joi.number().integer().min(0).required(),
                        remarks: Joi.string()
                    })
                }
            },
            handler: async (req: Request, res: ResponseToolkit) => {
                const { firstname, lastname, sprint, hours, remarks } = req.payload as any;
                await this.db.updatePersonSprintData(firstname, lastname, sprint, hours, remarks);

                return res.response().code(201);
            }
        });
    }
}