import {Server} from "@hapi/hapi";
import {DatabaseModule} from "../modules/database";

export abstract class Route {
    protected db: DatabaseModule;
    protected server: Server;

    constructor(server: Server, db: DatabaseModule) {
        this.db = db;
        this.server = server;

        this.setupRoutes();
        console.log(`[HAPI] Setup routes for ${this.constructor.name}`);
    }

    public abstract setupRoutes(): void;
}