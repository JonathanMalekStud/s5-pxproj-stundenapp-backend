import {Route} from "./route";
import {Request, ResponseToolkit} from "@hapi/hapi";
import * as Joi from "joi";

export class PersonRoute extends Route {
    setupRoutes(): void {
        this.server.route({
            method: "GET",
            path: "/person/{lastname}",
            options: {
                validate: { params: { lastname: Joi.string().required() } }
            },
            handler: async (req: Request, res: ResponseToolkit) => {
                return await this.db.getPerson(req.params.lastname);
            }
        });
    }
}